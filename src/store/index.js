import { createStore } from "vuex";
import moduleUi from "./modules/ui";
import moduleAuth from "./modules/auth";
import moduleUser from "./modules/user";
import moduleFinances from "./modules/finances";
import modulePartners from "./modules/partners";
import moduleRecruits from "./modules/recruits";

export default createStore({
  modules: {
    ui: moduleUi,
    auth: moduleAuth,
    user: moduleUser,
    finances: moduleFinances,
    partners: modulePartners,
    recruits: moduleRecruits,
  },

  actions: {
    RESET_STATE: ({ commit }) => {
      commit("ui/RESET_STATE", null, { root: true });
      commit("auth/RESET_STATE", null, { root: true });
      commit("user/RESET_STATE", null, { root: true });
      commit("finances/RESET_STATE", null, { root: true });
      commit("partners/RESET_STATE", null, { root: true });
      commit("recruits/RESET_STATE", null, { root: true });
    },
  },
});
