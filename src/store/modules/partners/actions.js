import { fetchPartners } from "@/api/partners";

export default {
  FETCH_PARTNERS({ state, commit }, uid) {
    commit("SET_FETCHING_STATE", true);
    commit("SET_FETCHING_ERROR", false);

    if (state.items.lenght > 0) {
      return;
    }

    return fetchPartners(uid)
      .then((partners) => {
        commit("SET_ITEMS", partners);
        commit("SET_FETCHING_STATE", false);
      })
      .catch((error) => {
        commit("SET_FETCHING_STATE", false);
        commit("SET_FETCHING_ERROR", true);
        console.error(error);
      });
  },
};
