import initialState from "./initial-state";

export default {
  SET_ITEMS(state, payload) {
    state.items = payload;
  },

  SET_FETCHING_STATE(state, payload) {
    state.isFetching = payload;
  },

  SET_FETCHING_ERROR(state, payload) {
    state.hasFetchingError = payload;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
