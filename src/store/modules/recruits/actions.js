import { fetchRecruits } from "@/api/recruits";

export default {
  FETCH_RECRUITS({ state, commit }, uid) {
    commit("SET_FETCHING_STATE", true);
    commit("SET_FETCHING_ERROR", false);

    if (state.items.lenght > 0) {
      return;
    }

    return fetchRecruits(uid)
      .then((recruits) => {
        commit("SET_ITEMS", recruits);
        commit("SET_FETCHING_STATE", false);
      })
      .catch((error) => {
        commit("SET_FETCHING_STATE", false);
        commit("SET_FETCHING_ERROR", true);
        console.error(error);
      });
  },
};
