import { recoverEmail, verifyEmail, sendPasswordResetLink } from "@/api/auth";

export default {
  RECOVER_EMAIL({ commit }, code) {
    let recoveredEmail = null;

    commit("CLEAR_ALL_ERRORS");
    commit("SET_PROCESSING", true);

    return recoverEmail(code)
      .then((email) => {
        recoveredEmail = email;
      })
      .then(() => verifyEmail(code))
      .then(() => sendPasswordResetLink(recoveredEmail))
      .then(() => {
        commit("SET_PROCESSING", false);
        commit("SET_POPUP_OPENED_STATUS", false);
        commit("SET_RESULT", true);

        return true;
      })
      .catch((error) => {
        commit("SET_PROCESSING", false);
        commit("PUSH_ERROR", error.code.replace("auth/", ""));

        return false;
      });
  },
};
