import { sendEmailVerificationCode, verifyEmail } from "@/api/auth";

export default {
  SEND_EMAIL_VERIFICATION_CODE({ commit }) {
    commit("SET_PROCESSING", true);
    commit("CLEAR_ALL_ERRORS");

    return sendEmailVerificationCode()
      .then(() => {
        commit("SET_CODE_SENDING_RESULT", true);
        commit("SET_PROCESSING", false);

        return true;
      })
      .catch((error) => {
        console.error("error:", error);

        commit("SET_ERRORS", error);
        commit("SET_PROCESSING", false);

        return false;
      });
  },

  VERIFY_EMAIL({ commit }, actionCode) {
    return verifyEmail(actionCode)
      .then(() => {
        commit("SET_RESULT", true);
        commit("user/UPDATE_USER", { isEmailVerified: true }, { root: true });

        return true;
      })
      .catch((error) => {
        commit("PUSH_ERROR", error.code.replace("auth/", ""));

        return false;
      });
  },
};
