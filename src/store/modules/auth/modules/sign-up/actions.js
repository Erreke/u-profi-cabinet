import { register } from "@/api/auth";

export default {
  SIGN_UP({ commit, dispatch }, { email, password, recruiterId }) {
    let user = null;

    commit("SET_PROCESSING", true);
    commit("CLEAR_ALL_ERRORS");

    commit("auth/sign-in/CLEAR_ALL_ERRORS", null, { root: true });
    commit("auth/email-verification/CLEAR_ALL_ERRORS", null, { root: true });
    commit("auth/remind-password/CLEAR_ALL_ERRORS", null, { root: true });
    commit("auth/reset-password/CLEAR_ALL_ERRORS", null, { root: true });
    // commit("auth/change-email/CLEAR_ALL_ERRORS", null, { root: true });
    // commit("auth/change-phone/CLEAR_ALL_ERRORS", null, { root: true });
    // commit("auth/change-password/CLEAR_ALL_ERRORS", null, { root: true });

    commit("auth/sign-in/SET_RESULT", false, { root: true });
    commit("auth/email-verification/SET_RESULT", false, { root: true });
    commit("auth/remind-password/SET_RESULT", false, { root: true });
    commit("auth/reset-password/SET_RESULT", false, { root: true });
    // commit("auth/change-email/SET_RESULT", false, { root: true });
    // commit("auth/change-phone/SET_RESULT", false, { root: true });
    // commit("auth/change-password/SET_RESULT", false, { root: true });

    return register(email, password)
      .then((response) => {
        user = response.user;

        return dispatch(
          "user/CREATE_USER_PROFILE",
          {
            uid: user.uid,
            data: {
              recruiterId,
            },
          },
          {
            root: true,
          }
        );
      })
      .then(() => {
        commit("SET_PROCESSING", false);
        commit("SET_RESULT", true);

        return dispatch("user/FETCH_USER_PROFILE", user, {
          root: true,
        });
      })
      .catch((error) => {
        commit("SET_PROCESSING", false);
        commit("PUSH_ERROR", error.code.replace("auth/", ""));

        return false;
      });
  },
};
