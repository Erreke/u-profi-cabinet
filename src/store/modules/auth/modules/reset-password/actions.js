import { checkPasswordResetCode, saveNewPassword } from "@/api/auth";

export default {
  CHECK_PASSWORD_RESET_CODE({ commit }, code) {
    commit("SET_CODE_CHECK_PROCESSING", true);

    return checkPasswordResetCode(code)
      .then((email) => {
        commit("SET_RESETTING_PASSWORD_EMAIL", email);
        commit("SET_CODE_CHECK_PROCESSING", false);

        return true;
      })
      .catch((error) => {
        commit("SET_CODE_CHECK_PROCESSING", false);
        commit("PUSH_ERROR", error.code.replace("auth/", ""));
      });
  },

  SAVE_NEW_PASSWORD({ commit }, { code, password }) {
    commit("SET_PROCESSING", true);

    return saveNewPassword(code, password)
      .then(() => {
        commit("SET_RESULT", true);
        commit("SET_PROCESSING", false);

        return true;
      })
      .catch((error) => {
        commit("SET_PROCESSING", false);
        commit("PUSH_ERROR", error.code.replace("auth/", ""));

        return false;
      });
  },
};
