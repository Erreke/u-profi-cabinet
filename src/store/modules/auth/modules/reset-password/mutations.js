import protoMutations from "@/store/modules/auth/proto-mutations";

export default {
  ...protoMutations(),

  SET_CODE_CHECK_PROCESSING(state, payload) {
    state.isCodeChecking = payload;
  },

  SET_RESETTING_PASSWORD_EMAIL(state, payload) {
    state.resettingPasswordEmail = payload;
  },
};
