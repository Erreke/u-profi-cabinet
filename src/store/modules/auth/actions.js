import { exit } from "@/api/auth";

export default {
  SIGN_OUT: ({ dispatch }) =>
    exit().then(() => dispatch("RESET_STATE", null, { root: true })),
};
