import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import signInModule from "./modules/sign-in";
import signUpModule from "./modules/sign-up";
import emailVerificationModule from "./modules/email-verification";
import remindPasswordModule from "./modules/remind-password";
import resetPasswordModule from "./modules/reset-password";
import recoverEmailModule from "./modules/recover-email";
// import changePasswordModule from "./modules/change-password";
// import changeEmailModule from "./modules/change-email";
// import changePhoneModule from "./modules/change-phone";

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  modules: {
    "sign-in": signInModule,
    "sign-up": signUpModule,
    "email-verification": emailVerificationModule,
    "remind-password": remindPasswordModule,
    "reset-password": resetPasswordModule,
    "recover-email": recoverEmailModule,
    // "change-password": changePasswordModule,
    // "change-email": changeEmailModule,
    // "change-phone": changePhoneModule,
  },
};
