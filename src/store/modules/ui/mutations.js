import initialState from "./initial-state";

export default {
  TOGGLE_SIDENAV_ON_MOBILE(state) {
    state.isSidenavActiveOnMobile = !state.isSidenavActiveOnMobile;
  },

  HIDE_SIDENAV_ON_MOBILE(state) {
    state.isSidenavActiveOnMobile = false;
  },

  TOGGLE_SIDENAV_ON_DESKTOP(state) {
    state.isSidenavInactiveOnDesktop = !state.isSidenavInactiveOnDesktop;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
