export default {
  isSidenavActiveOnMobile: false,
  isSidenavInactiveOnDesktop: false,
};
