import { fetchFinances } from "@/api/finances";

export default {
  FETCH_FINANCES({ state, commit }, uid) {
    commit("SET_FETCHING_STATE", true);
    commit("SET_FETCHING_ERROR", false);

    if (state.items.lenght > 0) {
      commit("SET_FETCHING_STATE", false);
      return;
    }

    return fetchFinances(uid)
      .then((finances) => {
        commit("SET_ITEMS", finances);
        commit("SET_FETCHING_STATE", false);
      })
      .catch((error) => {
        commit("SET_FETCHING_STATE", false);
        commit("SET_FETCHING_ERROR", true);
        console.error(error);
      });
  },
};
