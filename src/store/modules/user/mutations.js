import initialState from "./initial-state";

export default {
  SET_USER(state, payload) {
    state.profile = payload;
  },

  UPDATE_USER(state, payload) {
    state.profile = {
      ...state.profile,
      ...payload,
    };
  },

  SET_PROFILE_CHANGING_PROCESS(state, payload) {
    state.isProfileChanging = payload;
  },

  SET_PROFILE_SAVING_PROCESS(state, payload) {
    state.isProfileSaving = payload;
  },

  SET_AVATAR_SAVING_PROCESS(state, payload) {
    state.isAvatarSaving = payload;
  },

  SET_RECRUITER_EXIST_CHECK_PROCESSING(state, payload) {
    state.isRecruiterExistCheckProcessing = payload;
  },

  SET_RECRUITER_EXIST_CHECKING_RESULT(state, payload) {
    state.recruiterExistCheckingResult = payload;
  },

  SET_RECRUITER_EXIST_CHECKING_ERROR(state, payload) {
    state.recruiterExistCheckingError = payload;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
