export default {
  profile: null,
  isProfileChanging: false,
  isProfileSaving: false,
  isAvatarSaving: false,
  isRecruiterExistCheckProcessing: true,
  recruiterExistCheckingResult: null,
  recruiterExistCheckingError: null,
};
