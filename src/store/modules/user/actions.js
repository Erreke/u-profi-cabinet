import {
  createUserProfile,
  fetchUserProfile,
  isRecruiterExist,
  saveUserInfo,
} from "@/api/profile";

function composeUserProfile(user, userData) {
  return {
    // data from Firebase.Auth
    uid: user.uid,
    email: user.email,
    phone: user.phoneNumber,
    isEmailVerified: user.emailVerified,
    name: user.displayName,
    avatar: user.photoURL,
    createdAt: user.metadata.createdAt,
    lastLoginAt: user.metadata.lastLoginAt,
    passwordUpdatedAt: user.reloadUserInfo?.passwordUpdatedAt,

    // data from Firestore 'profiles' table
    ...userData,
  };
}

export default {
  CREATE_USER_PROFILE(context, { uid, data }) {
    return createUserProfile(uid, data);
  },

  FETCH_USER_PROFILE({ commit }, user) {
    return fetchUserProfile(user.uid).then((userData) => {
      const userProfile = composeUserProfile(user, userData);

      commit("SET_USER", userProfile);
      commit("auth/SET_POPUP_OPENED_STATUS", false, { root: true });
      commit("auth/sign-in/SET_PROCESSING", false, { root: true });

      return userProfile;
    });
  },

  IS_RECRUITER_EXIST({ commit }, uid) {
    commit("SET_RECRUITER_EXIST_CHECK_PROCESSING", false);

    return isRecruiterExist(uid)
      .then((result) => {
        commit("SET_RECRUITER_EXIST_CHECK_PROCESSING", false);
        commit("SET_RECRUITER_EXIST_CHECKING_RESULT", result);
      })
      .catch((error) => {
        commit("SET_RECRUITER_EXIST_CHECK_PROCESSING", false);
        commit("SET_RECRUITER_EXIST_CHECKING_ERROR", error);

        console.error(error);
      });
  },

  SAVE_USER_INFO({ commit }, { uid, fields, values, loader }) {
    if (loader) {
      commit(loader, true, { root: true });
    }

    return saveUserInfo(uid, fields, values).then(() => {
      if (loader) {
        commit(loader, false, { root: true });
      }

      return true;
    });
  },
};
