import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/pages/Home.vue";
import NotFound from "../views/pages/NotFound.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: {
      layout: "light",
      metaTitle: "Главная",
    },
  },
  {
    path: "/sign-in",
    name: "sign-in",
    component: () =>
      import(/* webpackChunkName: "sign-in" */ "../views/pages/SignIn.vue"),
    meta: {
      isAnonOnly: true,
      layout: "light",
      metaTitle: "Авторизация",
    },
  },
  {
    path: "/sign-up",
    name: "sign-up",
    component: () =>
      import(/* webpackChunkName: "sign-up" */ "../views/pages/SignUp.vue"),
    meta: {
      isAnonOnly: true,
      layout: "light",
      metaTitle: "Регистрация",
    },
  },
  {
    path: "/__/auth/action",
    name: "auth-action",
    component: () =>
      import(
        /* webpackChunkName: "auth-action" */ "../views/pages/AuthAction.vue"
      ),
    meta: {
      layout: "light",
      metaTitle: "Перенаправление",
    },
  },
  {
    path: "/verify-email/:code",
    name: "verify-email",
    component: () =>
      import(
        /* webpackChunkName: "verify-email" */ "../views/pages/VerifyEmail.vue"
      ),
    props: true,
    meta: {
      layout: "light",
      metaTitle: "Подтверждение электронной почты",
    },
  },
  {
    path: "/remind-password",
    name: "remind-password",
    component: () =>
      import(
        /* webpackChunkName: "remind-password" */ "../views/pages/RemindPassword.vue"
      ),
    meta: {
      isAnonOnly: true,
      layout: "light",
      metaTitle: "Вспомнить пароль",
    },
  },
  {
    path: "/reset-password/:code",
    name: "reset-password",
    component: () =>
      import(
        /* webpackChunkName: "reset-password" */ "../views/pages/ResetPassword.vue"
      ),
    props: true,
    meta: {
      isAnonOnly: true,
      layout: "light",
      metaTitle: "Восстановить пароль",
    },
  },
  {
    path: "/recover-email/:code",
    name: "recover-email",
    component: () =>
      import(
        /* webpackChunkName: "recover-email" */ "../views/pages/RecoverEmail.vue"
      ),
    props: true,
    meta: {
      isAnonOnly: true,
      layout: "light",
      metaTitle: "Восстановить е-мейл",
    },
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: () =>
      import(
        /* webpackChunkName: "dashboard" */ "../views/pages/Dashboard.vue"
      ),
    meta: {
      isAuthOnly: true,
      isShownInMenu: true,
      layout: "default",
      metaTitle: "Дашборд",
      pageTitle: "Мониторинг всех показателей",
      icon: "view_quilt",
    },
  },
  {
    path: "/finances",
    name: "finances",
    component: () =>
      import(/* webpackChunkName: "finances" */ "../views/pages/Finances.vue"),
    meta: {
      isAuthOnly: true,
      isShownInMenu: true,
      layout: "default",
      metaTitle: "Мои финансы",
      pageTitle: "Мониторинг финансовых показателей",
      icon: "account_balance_wallet",
    },
  },
  {
    path: "/partners",
    name: "partners",
    component: () =>
      import(/* webpackChunkName: "partners" */ "../views/pages/Partners.vue"),
    meta: {
      isAuthOnly: true,
      isShownInMenu: true,
      layout: "default",
      metaTitle: "Мои партнеры",
      pageTitle: "Мониторинг партнерских показателей",
      icon: "business_center",
    },
  },
  {
    path: "/recruits",
    name: "recruits",
    component: () =>
      import(/* webpackChunkName: "recruits" */ "../views/pages/Recruits.vue"),
    meta: {
      isAuthOnly: true,
      isShownInMenu: true,
      layout: "default",
      metaTitle: "Мои рекруты",
      pageTitle: "Мониторинг показателей по рекрутам",
      icon: "group",
    },
  },
  {
    path: "/profile",
    name: "profile",
    component: () =>
      import(/* webpackChunkName: "profile" */ "../views/pages/Profile.vue"),
    meta: {
      isAuthOnly: true,
      isShownInMenu: true,
      layout: "default",
      metaTitle: "Мой профиль",
      pageTitle: "Здесь можно поменять данные профиля",
      icon: "person",
    },
  },
  {
    path: "/:catchAll(.*)",
    name: "not-found",
    component: NotFound,
    meta: {
      layout: "default",
      metaTitle: "Ошибка 404",
      pageTitle: "Ошибка 404",
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
