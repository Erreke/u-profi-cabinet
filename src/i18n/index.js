import { createI18n } from "vue-i18n/index";

import kz from "./locales/kz.json";
import ru from "./locales/ru.json";
import gb from "./locales/gb.json";
import tr from "./locales/tr.json";

let i18n;

if (!i18n) {
  i18n = createI18n({
    legacy: true,
    locale: "ru",
    messages: {
      kz,
      ru,
      gb,
      tr,
    },
  });
}

export default i18n;
