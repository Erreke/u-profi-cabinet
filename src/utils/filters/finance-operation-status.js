export default function (value) {
  // ID операции - uid
  // Сумма - amount

  // Бонус за покупку партнером - isBonusForPartnerPurchase
  // Бонус за приглашение рекрута - isBonusForRecruit
  // Перевод средств внутри системы - isTransfer
  // Вывод средств на карту - isWithdrawal

  // Выполнен - isCompleted && !isDeclined
  // Отклонен - !isCompleted && isDeclined

  // В ожидании - !isCompleted && !isDeclined

  // Дата создания - createdAt
  // Дата выполнения - completedAt
  // Дата отклонения - declinedAt

  if (value.isCompleted && !value.isDeclined) {
    return `<span class="status success">Выполнен</span>`;
  }

  if (!value.isCompleted && value.isDeclined) {
    return `<span class="status declined">Отклонен</span>`;
  }

  if (!value.isCompleted && !value.isDeclined) {
    return `<span class="status pending">В ожидании</span>`;
  }

  return `<span class="status new">Создан</span>`;
}
