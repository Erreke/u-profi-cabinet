export default function (value) {
  // ID операции - uid
  // Сумма - amount

  // Бонус за покупку партнером - isBonusForPartnerPurchase
  // Бонус за приглашение рекрута - isBonusForRecruit
  // Вывод средств на карту - isWithdrawal
  // Перевод средств внутри системы - isTransfer

  // Выполнен - isCompleted && !isDeclined
  // Отклонен - !isCompleted && isDeclined

  // В ожидании - !isCompleted && !isDeclined

  // Дата создания - createdAt
  // Дата выполнения - completedAt
  // Дата отклонения - declinedAt

  if (value.isBonusForPartnerPurchase) {
    return "Бонус за покупку партнером";
  }

  if (value.isBonusForRecruit) {
    return "Бонус за приглашение рекрута";
  }

  if (value.isWithdrawal) {
    return "Вывод средств на карту";
  }

  if (value.isTransfer) {
    return "Перевод средств внутри системы";
  }

  return "Неизвестно";
}
