import { defineAsyncComponent } from "vue";

export default function (componentName) {
  // TODO: generate webpackChunkName dinamically

  return defineAsyncComponent(() =>
    import(`@/views/components/${componentName}.vue`)
  );
}
