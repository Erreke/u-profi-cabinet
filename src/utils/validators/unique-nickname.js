import userAPI from '@/api/user';

export default function uniqueNickname(value, others) {
  if (value === '') return true;
  if (value === others.ownNickname) return true;

  const result = userAPI.isUniqueNickname(value)
    .then(response => response)
    // eslint-disable-next-line no-console
    .catch(e => console.error(e));

  return result;
}
