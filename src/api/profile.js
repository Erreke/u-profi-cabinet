import "@/firebase";

import {
  getFirestore,
  // collection,
  // getDocs,
  doc,
  setDoc,
  getDoc,
} from "firebase/firestore";

const db = getFirestore();

async function createUserProfile(uid, data) {
  const docRef = doc(db, "profiles", uid);

  return await setDoc(docRef, { ...data }, { merge: true });
}

async function fetchUserProfile(uid) {
  const docRef = doc(db, "profiles", uid);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    return docSnap.data();
  } else {
    return {};
  }
}

async function isRecruiterExist(uid) {
  const docRef = doc(db, "availableRefs", "refs");
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    const refs = docSnap.data();

    return refs.available.includes(uid);
  }

  return false;
}

function saveUserInfo(uid, fields, values) {
  const data = {};

  fields.forEach((field, index) => {
    data[field] = values[index];
  });

  return db.collection("profiles").doc(uid).update(data);
}

export { createUserProfile, fetchUserProfile, isRecruiterExist, saveUserInfo };
