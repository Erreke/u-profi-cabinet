import "@/firebase";

import {
  getAuth,
  onAuthStateChanged,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  sendEmailVerification,
  applyActionCode,
  checkActionCode,
  sendPasswordResetEmail,
  verifyPasswordResetCode,
  confirmPasswordReset,
  signOut,
} from "firebase/auth";

const auth = getAuth();

function checkAuth() {
  return onAuthStateChanged;
}

function getCurrentUser() {
  return auth.currentUser;
}

function register(email, password) {
  return createUserWithEmailAndPassword(auth, email, password);
}

function login(email, password) {
  return signInWithEmailAndPassword(auth, email, password);
}

function sendEmailVerificationCode() {
  return sendEmailVerification(auth.currentUser);
}

function verifyEmail(code) {
  return applyActionCode(auth, code);
}

function sendPasswordResetLink(email) {
  return sendPasswordResetEmail(auth, email);
}

function checkPasswordResetCode(code) {
  return verifyPasswordResetCode(auth, code);
}

function saveNewPassword(code, password) {
  return confirmPasswordReset(auth, code, password);
}

async function recoverEmail(code) {
  const info = await checkActionCode(auth, code);
  return info["data"]["email"];
}

function exit() {
  return signOut(auth);
}

export {
  auth,
  checkAuth,
  getCurrentUser,
  register,
  login,
  sendEmailVerificationCode,
  verifyEmail,
  sendPasswordResetLink,
  checkPasswordResetCode,
  saveNewPassword,
  recoverEmail,
  exit,
};
