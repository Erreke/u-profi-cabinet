import "@/firebase";

import {
  getFirestore,
  collection,
  query,
  where,
  getDocs,
} from "firebase/firestore";

const db = getFirestore();

async function fetchRecruits(uid) {
  const q = query(collection(db, "recruits"), where("uid", "==", uid));
  const querySnapshot = await getDocs(q);

  const result = [];

  querySnapshot.forEach((doc) => {
    result.push({
      id: doc.id,
      ...doc.data(),
    });
  });

  return result;
}

export { fetchRecruits };
