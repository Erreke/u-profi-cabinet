import M from "materialize-css";
import { createApp } from "vue";
import { auth, checkAuth, getCurrentUser } from "@/api/auth";

// import "@/registerServiceWorker";

import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import i18n from "@/i18n";

import globalMixin from "@/utils/mixins/global";

import capitalizeFilter from "@/utils/filters/capitalize";
import currencyFormatFilter from "@/utils/filters/currency-format";
import dateFormatFilter from "@/utils/filters/date-format";
import dateTimeFormatFilter from "@/utils/filters/date-time-format";
import numberFormatFilter from "@/utils/filters/number-format";
import financeOperationTypeFilter from "@/utils/filters/finance-operation-type";
import financeOperationStatusFilter from "@/utils/filters/finance-operation-status";

let isRouterHooksInited = false;
let app;

function setMetaTitle(matched) {
  matched.forEach((m) => {
    document.title = `${m.meta.metaTitle} | Rubus`;
  });
}

function setPageClass(matched) {
  matched.forEach((m) => {
    document.body.className = `page_${m.name}`;
  });
}

function closeSideNav() {
  store.commit("ui/HIDE_SIDENAV_ON_MOBILE");
}

function initRouterHooks() {
  router.beforeEach((to, from, next) => {
    setMetaTitle(to.matched);
    setPageClass(to.matched);
    closeSideNav();

    const currentUser = getCurrentUser();

    const isAuthenticated = !!currentUser;
    const isAuthOnlyArea = to.matched.some((record) => record.meta.isAuthOnly);
    const isAnonOnlyArea = to.matched.some((record) => record.meta.isAnonOnly);

    const isAnonAttemptsAccessToAuthOnlyArea =
      !isAuthenticated && isAuthOnlyArea;
    const isUserAttemptsAccessToAnonOnlyArea =
      isAuthenticated && isAnonOnlyArea;

    // console.log('-------------- beforeEach ------------------');
    // console.log('isAuthenticated', isAuthenticated);
    // console.log('isAuthOnlyArea', isAuthOnlyArea);
    // console.log('isAnonOnlyArea', isAnonOnlyArea);

    // console.log('--------------------------------');
    // console.log('isAnonAttemptsAccessToAuthOnlyArea ', isAnonAttemptsAccessToAuthOnlyArea );
    // console.log('isUserAttemptsAccessToAnonOnlyArea', isUserAttemptsAccessToAnonOnlyArea);
    // console.log('--------------------------------');

    if (isAnonAttemptsAccessToAuthOnlyArea) {
      next({ name: "sign-in" });
    } else if (isUserAttemptsAccessToAnonOnlyArea) {
      next({ name: "home" });
    } else {
      next();
    }
  });

  isRouterHooksInited = true;
}

function initApp() {
  checkAuth()(auth, async (user) => {
    if (user) {
      await store.dispatch("user/FETCH_USER_PROFILE", user);
    }

    if (!isRouterHooksInited) initRouterHooks();

    if (!app) {
      app = createApp(App);

      app.config.globalProperties.$_filters = {
        capitalize: capitalizeFilter,
        currency: currencyFormatFilter,
        date: dateFormatFilter,
        dateTime: dateTimeFormatFilter,
        number: numberFormatFilter,
        financeOperationType: financeOperationTypeFilter,
        financeOperationStatus: financeOperationStatusFilter,
      };

      app.config.globalProperties.$_materialize = M;

      app.use(store).use(router).use(i18n).mixin(globalMixin).mount("#app");

      M.AutoInit();
    }
  });
}

initApp();
